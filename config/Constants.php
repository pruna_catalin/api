<?php

/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 16/02/2018
 * Time     : 9:27 AM
 */


namespace WD\Config;
class Constants{
	const DEV_MODE = false;
	const APP_NAME = "CreativeX System"; 
	const DOMAIN_NAME = "webdirect.ro";
	// COOKIE SETTINGS //
	const COOKIE_EXPIRE = 0; // mean never
	const COOKIE_NAME = "CreativeX_"; //
	const COOKIE_VALUE = "CreativeX_Val_"; // mean never
	const COOKIE_PATH = "/"; // mean never.
	const COOKIE_DOMAIN = "webdirect.ro"; // mean never
	const COOKIE_SECURE = false; // mean never
	const COOKIE_HTTPONLY = false; // mean never
	// COOKIE SETTINGS END//
	const LIVE_URL = "https://www.webdirect.ro/clients/nutritie/catalin/";
	const REDIRECT_URL = "https://www.webdirect.ro/clients/nutritie/catalin/";
	const ADMIN_REDIRECT_URL = "https://www.webdirect.ro/clients/nutritie/catalin/admin/";
	const ADMIN_URL = "https://www.webdirect.ro/clients/nutritie/catalin/admin/";
	const MAX_PERPAGE = 10;
	const MAX_PSPAGE = 5;
	const FRONT_THEME = "nutritie"; // name folder for THEME
	const BACK_THEME = "BS4"; // name folder for THEME
	const FRONT_THEME_ASSETS = self::LIVE_URL . "system/views/assets"; // name folder for THEME
	const SYSTEM_CONTROLLER = BASEPATH . "/system/core/controller"; // name folder for THEME
	const SYSTEM_MODEL = BASEPATH . "/system/core/model"; // name folder for THEME
	const SYSTEM_FILES = BASEPATH . "/system/files"; // name folder for THEME
	const SYSTEM_VIEWS = BASEPATH . "/system/views/"; // name folder for THEME
	const SYSTEM_IMAGES = BASEPATH . "/system/views/assets/images/"; // name folder for THEME
	const SYSTEM_IMAGES_CLIENT = BASEPATH . "/system/client/images/"; // name folder for client images
	const SYSTEM_VIEW = "";
	const SYSTEM_STORAGE = BASEPATH."/system/storage/";
	const DEV_RENDER_SCOPE = false;
	const MAX_PER_PAGE = 9;
	const SMTP_HOST = "";
	const SMTP_PORT	= "";
	const SMTP_USER = "";
	const SMTP_PASS = "";

	const EMAIL_OFFICE = "office.webdirectro@gmail.com";
	const GOOGLE_PUB_CAPTHA_KEY = "6LdqYp0UAAAAANPnBSsHPCMA4zrbvnjq0r-LyDmq";
	const GOOGLE_PRIV_CAPTHA_KEY = "6LcXPJgUAAAAAIGxjOow-CwzytSaLhDVFZDUxmQ3";

	const TRACKING_LIST = [0=>"FunCourier",1=>"Cargus",3 => "SameDay",4=>"NemoExpress"];
	const PROVIDERS_PATH = BASEPATH."/core/providers/";
	const SLUG_PERMISIONS = [
	        "see"=>"GET",
            "edit"=>"POST",
            "create"=>"PUT",
            "delete"=>"DELETE"
    ];
}
