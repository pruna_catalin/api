<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/25/2019
 * Time: 9:07 PM
 */
use WD\Core\Routes\Routes;
#==================================TRACKING SIDE=================================
Routes::getInstance()
    ->folder("tracking")
    ->keyRoute("tracking")
    ->admin(true)
    ->securityOff(false)
    ->post("tracking","TrackingController@trackingAction","XHR"); //TODO must put XHR
#==================================FEED SIDE=================================
Routes::getInstance()
    ->folder("feedme")
    ->keyRoute("feedme")
    ->admin(true)
    ->securityOff(false)
    ->post("feed","FeedmeController@prepareData","XHR"); //TODO must put XHR

#==================================ADMIN SIDE=================================
Routes::getInstance()
	->folder("admin")
	->keyRoute("dashboard")
	->admin(true)
	->securityOff(false)
	->post("sandbox","DashBoardController@actionView","XHR");
Routes::getInstance()
    ->folder("admin")
    ->keyRoute("dashboard")
    ->admin(true)
    ->securityOff(false)
    ->post("/dashboard","DashBoardController@actionView","XHR");
Routes::getInstance()
	->folder("admin")
	->keyRoute("dashboard")
	->admin(true)
	->securityOff(false)
	->post("users/authenticate","LoginController@actionLogin","XHR");

Routes::getInstance()
    ->folder("admin")
    ->keyRoute("dashboard")
    ->admin(true)
    ->securityOff(false)
    ->post("heartbleed","LoginController@actionHeartBleed","XHR");


Routes::getInstance()
    ->folder("admin")
    ->keyRoute("dashboard")
    ->admin(false)
    ->securityOff(false)
    ->get("checkIntegrity","CheckIntegrityController@check","");
#==================================ADMIN SIDE CHECK INTEGRITY=================================
Routes::getInstance()
	->folder("admin")
	->keyRoute("dashboard")
	->admin(true)
	->securityOff(false)
	->post("checkIntegrity","CheckIntegrityController@check","XHR");
Routes::getInstance()
    ->folder("pontaj")
    ->keyRoute("dashboard")
    ->admin(true)
    ->securityOff(false)
    ->get("pontajLogin","PontajController@testLogin","");

Routes::getInstance()
	->folder("admin")
	->keyRoute("dashboard")
	->admin(true)
	->securityOff(false)
	->post("pontajLogin","PontajController@check","XHR");



