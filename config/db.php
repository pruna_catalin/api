<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/25/2019
 * Time: 8:39 PM
 */
$config['database'] = [
	'hostname' => 'webdirect.ro',
	'username' => 'webdirec_sandbox',
	'dbname' => 'webdirec_cmp',
	'password' => 'passwordA1.',
	'port' => 3306,
	'init' => [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		PDO::ATTR_EMULATE_PREPARES => false,
		PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
	]
];


?>
