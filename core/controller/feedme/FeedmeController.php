<?php

namespace WD\Core\Controller\Feedme;

use WD\Config\Constants;
class FeedmeController
{
    private $params = [];
    private $url = "https://ngs-ro.nsoft.com/app.php/api_open/retail/events_filter.json";
    private $id = "c06af179-6545-49b3-9e17-fdf8d5ad76fe";
    public $product_name = "luckysix";
    public $count = 500;
    public $page = 1;
    public $start = "";
    public $end  = "";
    public function __construct($params){
        $this->params = $params;
    }
    public function prepareData(){
        if(isset($this->params->Args['start'])){
            $this->start = $this->params->Args['start']. " 12:00:00 am";
        }else{
            $this->start = date("Y-m-d",time()). " 12:00:00 am";
        }
        if(isset($this->params->Args['end'])){
            $this->end = $this->params->Args['end']. " 11:59:59 pm";
        }else{
            $this->start = date("Y-m-d",time()). " 11:59:59 pm";
        }
        if(isset($this->params->Args['product_name'])){
            $this->product_name = $this->params->Args['product_name'];
        }
        if(isset($this->params->Args['count'])){
            $this->count = $this->params->Args['count'];
        }
        if(isset($this->params->Args['page'])){
            $this->page = $this->params->Args['page'];
        }
        if(isset($this->params->Args['save_local'])){
            $this->_feedMe($this->params->Args['save_local']);
        }else{
            $this->_feedMe();
        }
    }
    public function _feedMe($local = false){
        $url = $this->url."?product=".$this->product_name."&count=".$this->count."&page=".$this->page."&timeFrom=".$this->start."&timeTo=".$this->end."&cpvUuid=".$this->id;
        $result = \Utils::download($url,"",$local);
        FillJSON([$result['content']]);
    }

}
