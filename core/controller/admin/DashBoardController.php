<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/17/2019
 * Time: 10:42 PM
 */

namespace WD\Core\Controller\Admin;

use WD\Tools\Storage;
use WD\Config\Constants;


class DashBoardController{
	private $params = [];
	private $storage = [];
	public function __construct($params){
		$this->params = $params;
		$this->storage = new Storage();
	}
	public function actionView(){
		$object = ["id"];

        return ["success"=>true,"data"=>$object];
	}

}
