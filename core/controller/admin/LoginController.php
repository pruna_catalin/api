<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 30/03/2018
 * Time     : 12:49 PM
 */
namespace WD\Core\Controller\Admin;
use WD\Core\View\Render;
use WD\Tools\Storage;
use WD\Config\Constants;
use WD\Core\Rules\Validation;
class LoginController
{
    private $storage = "";
    private $params = [];
    public function __construct($params){
        $this->storage = new Storage();
        $this->params = $params;
    }
    private function rules(){
        $rules = new Validation();
        $element = [
            "username"=>["required"=>true],
            "password"=>["required"=>true,"min"=>5,"regex"=>"\w+"]
        ];
        return $rules->formValidation($element,$this->params->Args['Model']);
//        return ["status"=>true];
    }
    public function actionLogout(){
    	$this->storage->session->flashSession();
	}
	public function actionHeartBleed(){
        FillJSON(["success"=>true,"message"=>"I'm alive keep your mind clear and drink beer"]);
    }
    public  function actionLogin(){
        $login = false;
        $errors = [];
        $messages = [];
        $status = "";
        if($this->params->Method == "POST"){
            $checkRule = $this->rules();
            if($checkRule['status']){
                $model = \Model::Users();
                $model->{"user_acc.login"} = $this->params->Args['Model']['username'];
                $dao = \DAO::Users("Find",$model);
                if($dao) {
                    if(password_verify($this->params->Args['Model']['password'], $dao->password)){
                        $token = \Utils::generateToken();
                        $modelToken = new \stdClass();
                        $modelToken->uid = $model->id;
                        $modelToken->session_client = 0;
                        $modelToken->session_token = $token;
                        $modelToken->session_addr = (isset($this->params->Args['Model']['ip_address']) ? $this->params->Args['Model']['ip_address'] : $_SERVER['SERVER_ADDR']);
                        $modelToken->ts_start = time();
                        $modelToken->ts_expire = time() + 3600;
                        $dao = \DAO::Users("InsertToken",$modelToken);
                        if($dao){
                            $messages['success'] = "Login Success";
                            FillJSON(["success"=>true,
                                "message"=>$messages['success'],
                                "user"=>[
                                    "username"=>$this->params->Args['Model']['username'],
                                    "token"=> $token
                                ],
                                "permisions"=>[
                                    "navbar"=>[

                                    ],
                                    "sidebar"=>[
                                        ["path"=>"/dashboard" , "title"=>"Dashboard", "icon"=>"dashboard", "class"=>"","id"=>"test1"],
                                        ["path"=>"/clocking" , "title"=>"Clocking", "icon"=>"av_timer", "class"=>""],
                                        ["path"=> "/users"      ,"title"=>"Users", "icon"=>"perm_identity", "class"=>"","id"=>"test2",
                                            "children"=>[
                                                ["title"=>"List users","path"=>"/list","icon"=>"list","class"=>"" ],
                                                ["title"=>"List activity","path"=>"/list_activity","icon"=>"directions_run","class"=>"" ],
                                            ]
                                        ],
                                        ["path"=>"/underB" , "title"=>"UnderB", "icon"=>"dashboard", "class"=>"","id"=>"test3",
                                            "children"=>[
                                                ["title"=>"List users2","path"=>"/list","icon"=>"list","class"=>"" ],
                                                ["title"=>"List activity2","path"=>"/list_activity","icon"=>"directions_run","class"=>"" ],
                                            ]
                                        ]

                                    ],
                                    "extra"=> [

                                    ]
                                ]
                            ]);
                        }else{
                            $messages['failed'] = "Insert token is broken";
                            FillJSON(["success"=>false,"message"=>$messages['failed']]);
                        }

                    }else{
                        $messages['failed'] = "Invalid password";
                        FillJSON(["success"=>false,"message"=>$messages['failed']]);
                    }

                }else{
					$messages['failed'] = "User or password is wrong";
                    FillJSON(["success"=>false,"message"=>$messages['failed']]);
                }
            }else{
                $messages['failed'] = $checkRule['Field']." ".$checkRule['Rule'] ;
                FillJSON(["success"=>false,"message"=>$messages['failed']]);
            }
        }
        FillJSON(["success"=>false,"message"=>"Method is not accepted"]);

    }
}
