<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/30/2018
 * Time: 12:21 AM
 */

namespace WD\Core\Controller\Admin;
use WD\Core\View\Render;
use WD\Tools\Storage;
use WD\Config\Constants;
class IndexController
{
	private $params = [];
	private $storage = [];
	public function __construct($params){
		$this->params = $params;
		$this->storage = new Storage();
	}

	public function Init($content){
        if (\Utils::JsonTest($content)) { // if response is json will send output JSON
            FillJSON($content);
        } else {

        }
	}
}
