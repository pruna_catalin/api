<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : catalin.p@rei-d-services.com
 * Skype    : catalin.p@rei-d-services.com
 * Date     : 18.06.2019
 * Time     : 11:33 AM
 */

namespace WD\Core\Controller\Admin;

use WD\Config\Constants;
use WD\Tools\Storage;
class CheckIntegrityController
{
    private $params = [];
    private $storage = [];
    public function __construct($params){
        $this->params = $params;
        $this->storage = new Storage();
    }
    /*
     * This function have 2 parts
     * 1.Check token expired date
     * 2.Check integrity of route
     */
    public function check(){
        $model = \Model::Users();
        $model->{"user_acc.login"} = (isset($this->params->Args['username']) ? $this->params->Args['username'] : "XnoUserFoundX");
        $dao = \DAO::Users('Find',$model);
        //try to find username
        if($dao){
            if(isset($this->params->Args['token'])){
                $daoToken = \DAO::Users("FindTokenUser",["username"=>$this->params->Args['username'],"session_token"=>$this->params->Args['token']]);
                if($daoToken){
                    //check token expired
                    if($daoToken->ts_expire < time()){
                        FillJSON(["success"=>false,"message"=>"Token expired","goTo"=>"Login"]);
                    }else{
                        //check route
                        FillJSON(["success"=>true,"message"=>"CheckRouting","goTo"=>""]);
                    }
                }else{
                    FillJSON(["success"=>false,"message"=>"User not found on token table","goTo"=>"Login"]);
                }
            }else{
                FillJSON(["success"=>false,"message"=>"Token not found","goTo"=>"Login"]);
            }
        }else{
            FillJSON(["success"=>false,"message"=>"User not found","goTo"=>"Login"]);
        }

    }
    private function checkPermision($slug, $permision){
        $methods = Constants::SLUG_PERMISIONS;

    }
}
