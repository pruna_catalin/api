<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : catalin.p@rei-d-services.com
 * Skype    : catalin.p@rei-d-services.com
 * Date     : 14.10.2019
 * Time     : 3:16 PM
 */
namespace WD\Core\Controller\Pontaj;

use WD\Config\Constants;
class PontajController
{
    public $login_url = "https://access-control.rei-d-services.com/authLoginAction!login.do";
    public $daily_breaks = "https://access-control.rei-d-services.com/attDayCardDetailReportAction!getAll.action";
    public $daily_report = "https://access-control.rei-d-services.com/attDayDetailReportAction!getAll.action";
    private $username = "1078";
    private $password = "697144";
    private $type_login = "PERS";
    private $systemCode = "login.jsp";
    public  $autologin  = 1;
    private $un = [];
    private $params = [];
    private $headers = [];
    public  $post = 1;
    public  $post_fields = [
    ];
    private $additional_headers = [    ];

    public function __construct($params){
        $time = time();
        $this->un[] = $time."_".rand(1000,9999);
        $this->un[] = ($time + 1 )."_".rand(1000,9999);
        $this->params = $params;
    }
    public function testLogin(){
        $this->params->Args['username'] = $this->username;
        $this->params->Args['password'] = $this->password;
        $this->accessDaily();

    }
    public function accessDaily(){
        $this->post_fields['packageName'] = "att";
        $this->post_fields['systemCode'] = "att";
        $this->post_fields['pager.posStart'] = "0";
        $this->post_fields['pager.pageSize'] = "50";
        $this->post_fields['xmlFileName'] = "AttDayCardDetailReport";
        if(isset($this->params->Args['beginTime'])) {
            $this->post_fields['beginTime'] = urlencode($this->params->Args['beginTime']);
        }else{
            $this->post_fields['beginTime'] = date("Y-m-01")."%2000:00:00";
        }
        if(isset($this->params->Args['endTime'])){
            $this->post_fields['endTime'] = urlencode($this->params->Args['endTime']);
        }else{
            $this->post_fields['endTime'] = date("Y-m-d")."%2023:59:59";
        }
        $response = json_decode($this->request($this->daily_breaks,'normal')['content']);
        FillJSON(["success"=>true,"data"=>$response]);
    }
    public function login(){
        if(isset($this->params->Args['loginType'])){
            $this->post_fields['loginType'] = $this->params->Args['loginType'];
        }else{
            $this->post_fields['loginType'] = $this->type_login;
        }
        if(isset($this->params->Args['username']) && isset($this->params->Args['password'])){
            $this->post_fields['username'] = trim($this->params->Args['username']);
            $this->post_fields['password'] = trim($this->params->Args['password']);
            $this->post_fields['autoLogin'] = $this->autologin;
            $response = json_decode($this->request($this->login_url,'login')['content']);
            if($response->ret == "ok"){
                FillJSON(["success"=>true,"message"=>"Login Success"]);
            }else{
                FillJSON(["success"=>false,"message"=>$response]);
            }
        }else{
            FillJSON(["success"=>false,"message"=>"Please provide username and password"]);
        }
    }
    public  function request($url,$type = 'normal'){
        $cookie = dirname(__FILE__) . '/cookie.txt';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch,CURLOPT_HEADER , 0);
        if($type == 'normal'){
            curl_setopt ($ch, CURLOPT_COOKIEFILE, $cookie);
        }else{
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        }
        if(empty($this->headers)){
            $headers = $this->additional_headers;
        }
        else{
            $headers = array_merge($this->additional_headers, $this->headers);
        }
        if($this->post){
            $post_arr = [];
            foreach ($this->post_fields as $key => $value) {
                $post_arr[] = $key.'='.$value;
            }
            $post_str = implode("&",$post_arr);
            $post_str .= "&un=".$this->un[0]."&systemCode=login.jsp&un=".$this->un[1]."&systemCode=login.jsp";
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_str);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return ['content'=>$result, 'info'=>$info, 'request'=>$this->post_fields];
    }
    //https://access-control.rei-d-services.com



}
