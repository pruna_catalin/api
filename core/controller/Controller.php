<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 14/02/2018
 * Time     : 11:01 AM
 */

namespace WD\Core\Controller;

use WD\Core\Controller\Admin\CheckIntegrityController;
use WD\Core\Routes\Routes;
use WD\Tools\Storage;
use WD\Core\Controller\Admin\LoginController;

require_once(BASEPATH . "/config/routes.php");

class Controller
{
	private $params = [];
	private $storage = [];

	public function __construct(){
		$this->storage = new Storage();
		$this->params = Routes::getInstance()->registerRoute();
	}

	/*
	 * This function will be call by API and receive params json format to send in view
	 * @params = will return by request
	 * @return  if(instanceof of View) ? HTML : (isJson) ? JsonFormat : result of function
	 *
	 */

	public function callController(){
		if ($this->params->Controller != "" && $_SERVER['REQUEST_METHOD'] == $this->params->Method) { //check exists controller
			if(!isset($this->params->Args["username"]) && !isset($this->params->Args["token"]) && !$this->params->Admin && $this->params->Action != "actionLogin" ){
                FillJSON(["success"=>false,"message"=>"Not Authorized","goTo"=>"Login"]);
			}else{
			    if($this->params->Action == "actionLogin"){
			           $l = new  LoginController($this->params);
			           $l->actionLogin();
                }else{
			        $check = new CheckIntegrityController($this->params);
			        if($this->params->Admin){
                        if (!empty($this->params->Folder) && $this->params->Folder != "")
                            $controllerString = __NAMESPACE__ . "\\" . ucfirst($this->params->Folder) . "\\" . $this->params->Controller; // try to create Class
                        else
                            $controllerString = __NAMESPACE__ . "\\" . $this->params->Controller; // try to create Class
                        if($this->params->Admin){
                            $response = new $controllerString($this->params); // try to use action from controller
                            $response->{$this->params->Action}(); // call function and return
                        }else{
                            FillJSON(["success"=>false,"message"=>"Not Authorized","goTo"=>"Login"]);
                        }
                    }else{
                        FillJSON(["success"=>false,"message"=>"Not Authorized","goTo"=>"NotFound"]);
                    }
                }
			}
		} else {
		    FillJSON(["success"=>false,"message"=>"NotFound","goTo"=>"NotFound"]);
		}

	}
}

