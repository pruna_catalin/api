<?php
namespace WD\Core\Controller\Tracking;

use WD\Config\Constants;
use WD\Core\Providers\Classes\FunCourier;
class TrackingController{

    private $params = [];
    public function __construct($params){
        $this->params = $params;
    }

    public function trackingAction(){
        if(isset($this->params->Args['code'])){
            if(array_key_exists($this->params->Args['code'],Constants::TRACKING_LIST)){
                if(Constants::TRACKING_LIST[$this->params->Args['code']] == "FunCourier"){
                    FunCourier::generateAWB($this->params->Args);
                }

            }else{
                FillJSON(["success"=>false,"message"=>"PROVIDER_NOT_FOUND","params"=>$this->params->Args]);
            }
        }else{
            FillJSON(["success"=>false,"message"=>"PROVIDER_NOT_SET"]);
        }
    }
}
