<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : catalin.p@rei-d-services.com
 * Skype    : catalin.p@rei-d-services.com
 * Date     : 20.09.2019
 * Time     : 10:46 AM
 */
namespace WD\Core\Model\DAO;
class DAOUsers{
    public static $order = "user_acc.id ASC";

    public static function Init($name,$args = [],$resultType = NULL){
        return self::{$name}($args,$resultType);
    }
    public static function TableName() {
        return "`cmp`.`user_accounts`";
    }
    public static function DBName() {
        return "cmp";
    }

    /*
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
     */
    public static function Find(/* user / role / p (permission) */$data, $resultType = NULL) {
        $db = new \AdvSql('');
        $return = [];
        $model  =  \Model::Users();
//        $db->showSql = true;
        if($data instanceof  $model){
            $where = \DAO::Tools("prepareCondition",$model);
            $db->select('user_acc.*,
                                role.slug as role_slug,
                                role.name as role_name,
                                GROUP_CONCAT(p.slug) as permission_slug,
                                GROUP_CONCAT(p.name) as permission_name
                                ')
                ->from("`".self::DBName()."`.`user_accounts` as user_acc")
                ->leftjoin("`".self::DBName()."`.`role_user` as user_role")->on("user_role.user_id = user_acc.id")
                ->leftJoin("`".self::DBName()."`.`roles` as role")->on("role.id = user_role.role_id")
                ->leftJoin("`".self::DBName()."`.`permission_role` as p_role")->on("p_role.role_id = role.id")
                ->leftJoin("`".self::DBName()."`.`permissions` as p")->on("p.id = p_role.permission_id")
                ->where($where)
                ->groupBy("user_acc.login")
                ->orderby(self::$order)
                ->prepare();

            $request = $db->execute();
            $result = $request->fetch($db::SwitchResult($resultType));
        }else  if ($data != "") {
            $db->select('user_acc.*,
                                role.slug as role_slug,
                                role.name as role_name,
                                GROUP_CONCAT(p.slug) as permission_slug,
                                GROUP_CONCAT(p.name) as permission_name
                                ')
                ->from("`".self::DBName()."`.`user_accounts` as user_acc")
                ->leftjoin("`".self::DBName()."`.`role_user` as user_role")->on("user_role.user_id = user_acc.id")
                ->leftJoin("`".self::DBName()."`.`roles` as role")->on("role.id = user_role.role_id")
                ->leftJoin("`".self::DBName()."`.`permission_role` as p_role")->on("p_role.role_id = role.id")
                ->leftJoin("`".self::DBName()."`.`permissions` as p")->on("p.id = p_role.permission_id")
                ->where($data)
                ->groupBy("user_acc.login")
                ->orderby(self::$order)
                ->prepare();

            $request = $db->execute();
            $result = $request->fetch($db::SwitchResult($resultType));

        }else{
            $db->select('user_acc.*,
                                role.slug as role_slug,
                                role.name as role_name,
                                GROUP_CONCAT(p.slug) as permission_slug,
                                GROUP_CONCAT(p.name) as permission_name
                                ')
                ->from("`".self::DBName()."`.`user_accounts` as user_acc")
                ->leftjoin("`".self::DBName()."`.`role_user` as user_role")->on("user_role.user_id = user_acc.id")
                ->leftJoin("`".self::DBName()."`.`roles` as role")->on("role.id = user_role.role_id")
                ->leftJoin("`".self::DBName()."`.`permission_role` as p_role")->on("p_role.role_id = role.id")
                ->leftJoin("`".self::DBName()."`.`permissions` as p")->on("p.id = p_role.permission_id")
                ->groupBy("user_acc.login")
                ->orderby(self::$order)
                ->prepare();
            $request = $db->execute();
            $result = $request->fetch($db::SwitchResult($resultType));
        }
        if($result){
            $model->id = $result->id;
            $model->login = $result->login;
            $model->password = $result->password;
            $model->first_name = $result->first_name;
            $model->last_name = $result->last_name;
            $model->email = $result->email;
            $model->status = $result->status;
            $model->ts_login = $result->ts_login;
            $model->ts_create = $result->ts_create;
            $model->password_reset_token = $result->password_reset_token;
            $model->password_reset_ttl = $result->password_reset_ttl;
            $model->role_slug = $result->role_slug;
            $model->role_name = $result->role_name;
            $model->permission_slug = $result->permission_slug;
            $model->permission_name = $result->permission_name;
            array_push($return,$model);
        }
        return (sizeof($return) > 0) ? $return[0] : false;
    }
    /*
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
     */
    public static function FindAll($data, $resultType = NULL){
        $db     = new \AdvSql('');
        $model  =  \Model::Users();
        $result = [];
        $returnData = [];
        if($data instanceof  $model){
            $where = \DAO::Tools("prepareCondition",$model);
            $db->select('user_acc.*,
                                role.slug as role_slug,
                                role.name as role_name,
                                GROUP_CONCAT(p.slug) as permission_slug,
                                GROUP_CONCAT(p.name) as permission_name
                                ')
                ->from("`".self::DBName()."`.`user_accounts` as user_acc")
                ->leftjoin("`".self::DBName()."`.`role_user` as user_role")->on("user_role.user_id = user_acc.id")
                ->leftJoin("`".self::DBName()."`.`roles` as role")->on("role.id = user_role.role_id")
                ->leftJoin("`".self::DBName()."`.`permission_role` as p_role")->on("p_role.role_id = role.id")
                ->leftJoin("`".self::DBName()."`.`permissions` as p")->on("p.id = p_role.permission_id")
                ->where($where)
                ->groupBy("user_acc.login")
                ->orderby(self::$order)
                ->prepare();
            $request = $db->execute();
            $result = $request->fetch($db::SwitchResult($resultType));
        }else  if ($data != "") {
            $db->select('user_acc.*,
                                role.slug as role_slug,
                                role.name as role_name,
                                GROUP_CONCAT(p.slug) as permission_slug,
                                GROUP_CONCAT(p.name) as permission_name
                                ')
                ->from("`".self::DBName()."`.`user_accounts` as user_acc")
                ->leftjoin("`".self::DBName()."`.`role_user` as user_role")->on("user_role.user_id = user_acc.id")
                ->leftJoin("`".self::DBName()."`.`roles` as role")->on("role.id = user_role.role_id")
                ->leftJoin("`".self::DBName()."`.`permission_role` as p_role")->on("p_role.role_id = role.id")
                ->leftJoin("`".self::DBName()."`.`permissions` as p")->on("p.id = p_role.permission_id")
                ->where($data)->orderby(self::$order)->prepare();
            $request = $db->execute();
            $result = $request->fetchAll($db::SwitchResult($resultType));
        }else{
            $db->select('user_acc.*,
                                role.slug as role_slug,
                                role.name as role_name,
                                GROUP_CONCAT(p.slug) as permission_slug,
                                GROUP_CONCAT(p.name) as permission_name
                                ')
                ->from("`".self::DBName()."`.`user_accounts` as user_acc")
                ->leftjoin("`".self::DBName()."`.`role_user` as user_role")->on("user_role.user_id = user_acc.id")
                ->leftJoin("`".self::DBName()."`.`roles` as role")->on("role.id = user_role.role_id")
                ->leftJoin("`".self::DBName()."`.`permission_role` as p_role")->on("p_role.role_id = role.id")
                ->leftJoin("`".self::DBName()."`.`permissions` as p")->on("p.id = p_role.permission_id")
                ->prepare();
            $request = $db->execute();
            $result = $request->fetchAll($db::SwitchResult($resultType));
        }
        foreach($result as $column){
            $model->id = $column->id;
            $model->login = $result->login;
            $model->password = $result->password;
            $model->first_name = $result->first_name;
            $model->last_name = $result->last_name;
            $model->email = $result->email;
            $model->status = $result->status;
            $model->ts_login = $result->ts_login;
            $model->ts_create = $result->ts_create;
            $model->password_reset_token = $result->password_reset_token;
            $model->password_reset_ttl = $result->password_reset_ttl;
            array_push($returnData,$model);
        }
        return $returnData;
    }

    /*
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
     */
    public static function FindTokenUser($data, $resultType = NULL){
        $db = new \AdvSql('');
        $db->bind([":login"=>$data['username'],":session_token"=>str_replace(" ","+",$data['session_token'])]);
        $db->select('session.*')
            ->from("`".self::DBName()."`.`user_accounts` as user_acc")
            ->leftJoin("`".self::DBName()."`.`sessions` as session")->on("session.uid = user_acc.id")
            ->where("user_acc.login = :login and session.session_token = :session_token")
            ->prepare();
//        $db->showSql = true;
        $request = $db->execute();
       return  $request->fetch($db::SwitchResult($resultType));
    }

    /*
     * @params  condition | MODEL
     * @ return false on FAILD
     */
    public static function Insert($model) {
        $dataParse = \DAO::Tools("InsertModel",$model);
        $data = array('input' => $dataParse[0]);
        if(sizeof($dataParse[0]) > 0 ){
            $sql = new \AdvSql($data);
            $sql->insert(self::tableName())->columns($dataParse[1])
                ->values($dataParse[2])->prepare();
            $sql->execute();
            return $sql->lastInsertId();
        }else{
            return FALSE;
        }
    }
    /*
     * @params  condition | MODEL
     * @ return false on FAILD
     */
    public static function InsertToken($model) {
        $dataParse = \DAO::Tools("InsertModel",$model);
        $data = array('input' => $dataParse[0]);
        if(sizeof($dataParse[0]) > 0 ){
            $sql = new \AdvSql($data);
            $sql->insert("`".self::DBName()."`.`sessions`")->columns($dataParse[1])
                ->values($dataParse[2])->prepare();
            $sql->execute();
            return $sql->lastInsertId();
        }else{
            return FALSE;
        }
    }
    /*
     * @params  condition | MODEL
     * @ return false on FAILD
     */
    public static function Update($model,$condition) {
        $where = \DAO::Tools("prepareCondition",$model);
        $dataParse = \DAO::Tools("UpdateModel",[$model,$condition]);
        if(sizeof($dataParse[1]) == 0){
            $data = $model;
        }else{
            $data = array('prepare' => array(':SET' =>$dataParse[0],':WHERE' => $where),
                'input' => $dataParse[1]);
        }
        $sql = new \AdvSql($data);
        $sql->update(self::tableName())->prepare();
        $result = $sql->execute();
        return ($result->rowCount() > 0 ) ? TRUE : FALSE;
    }

    public static function Delete($data) {
        $model  =  \Model::Users();
        $where = $data;
        if($data instanceof  $model) {
            $where = \DAO::Tools("prepareCondition", $model);
        }
        $sql = new \AdvSql('');
        $sql->delete("")->from(self::tableName())->where($where)->prepare();
        $result = $sql->execute();
        return ($result->rowCount() > 0 ) ? TRUE : FALSE;
    }

}
