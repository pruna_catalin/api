<?php
namespace WD\Core\Providers\Classes;

use WD\Config\Constants;

class FunCourier {

    public static function generateAWB($params){
        $provider = new \WD\Core\Providers\FunCourier($params);
        if($provider->validateData()){
            if(isset($params['fisier_location'])){
                if(strlen($params['fisier_location']) > 0){
                    $provider->post_fields['return'] = "services";
                    $provider->post_fields['keba'] = "1";
                    $provider->post_fields['ridicare'] = "1";
                    $provider->post_fields['greutate'] = "1";
                    $provider->post_fields['ramburs'] = "1";
                    $provider->post_fields['telefon'] = "0000000000";
                    $provider->post_fields['plata'] = "0000000000";
                    $provider->post_fields['data_istoric_cmd'] = "2019-09-28";
                    $result = $provider->request();
                    FillJSON(["success"=>true,"meesage"=>\Utils::readCSV($result['content']),"orginal_message"=>$result['content']]);
                }
            }else{
                FillJSON(["success"=>false,"message"=>"MISSING_FILE"]);
            }
        }else{
            FillJSON(["success"=>false,"message"=>"MISSING_MANDATORY_PARAMS"]);
        }
    }
}
