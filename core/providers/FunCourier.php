<?php
namespace WD\Core\Providers;

class FunCourier
{
    private $params = [];
    public  $post = 1;
    private $headers = [];
    private $sandbox = false;
    private $sandbox_clientID = 7032158;
    private $sandbox_username = "clienttest";
    private $sandbox_password = "testing";
    private $sandbox_url = "https://www.selfawb.ro/order.php";

    public $live_url = "https://www.selfawb.ro/order.php";

    public $requestPage = [
                            "generate_awb"=>"import_awb_integrat.php",
                            "generate_error_awb"=>"export_lista_erori_imp_awb_integrat.php",
                            "get_clients_list"=>"get_account_clients_integrat.php",
                            "get_awb"=>"view_awb_integrat.php",
                            "get_awb_pdf"=>"view_awb_integrat_pdf.php",
                            "delete_awb"=>"delete_awb_integrat.php",
                            "get_services"=>"export_servicii_integrat.php",
                            "get_routes"=>"export_distante_integrat.php",
                            "get_observations"=>"export_observatii_integrat.php",
                            "get_slip"=>"export_borderou_integrat.php",
                            "finish_slip"=>"finalizare_borderou_integrat.php",
                            "download_awb"=>"download_awb_scan_integrat.php",
                            "get_transactions"=>"export_raport_viramente_integrat.php",
                            "get_commands"=>"export_comenzi_integrat.php",
                            "get_street_postal_code"=>"export_strazi_integrat.php",
                            "get_courier"=>"comanda_curier_integrat.php",
                            "get_price"=>"tarif.php",
                            "awb_tracking"=>"awb_tracking_integrat.php",
                            "awb_tracking_list"=>"awb_tracking_list_integrat.ph",
    ];



    public $clientID = 0;
    public $username = "";
    public $password = "";
    public $url = "";
    public $post_fields = [];
    public function __construct($params,$sandbox = true){
        $this->sandbox = $sandbox;
        $this->params = $params;
        $this->validateData();
    }

    public function validateData(){
        $result = false;
        if(isset($this->params['clientID']) && isset($this->params['username']) && isset($this->params['password'])){
            if(is_numeric($this->params['clientID']) && strlen($this->params['username']) > 3 && strlen($this->params['password']) > 4){
                if($this->sandbox){
                    $this->clientID = $this->sandbox_clientID;
                    $this->username = $this->sandbox_username;
                    $this->password = $this->sandbox_password;
                    $this->url = $this->sandbox_url;
                }else{
                    $this->clientID = $this->params['clientID'];
                    $this->username = $this->params['username'];
                    $this->password = $this->params['user_pass'];
                    if(isset($this->params['request'])){
//                        $this->url = $this->live_url.$this->requestPage[$this->params['request']];
                    }
                }
                $this->post_fields['client_id'] = $this->clientID;
                $this->post_fields['username'] = $this->username;
                $this->post_fields['user_pass'] = $this->password;
                $result = true;
            }
        }
        return $result;
    }
    public  function request($special = false){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
//        if($special){
//            $boundary = uniqid();
//            $delimiter = '-------------' . $boundary;
//            $first_header[] = "Content-Type: multipart/form-data; boundary=".$delimiter;
//            $first_header[] = "Content-Length:".strlen(implode("&",$this->post_fields));
//            $this->headers = $first_header;
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
//
//        }
//        FillJSON([http_build_query($this->post_fields)]);exit;
        if($this->post){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->post_fields));
        }
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return ['content'=>$result, 'info'=>$info, 'request'=>[$this->post_fields,$this->headers]];
    }


}
