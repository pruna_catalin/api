<?php
use PHPMailer\PHPMailer\PHPMailer;

class DebugEmail{
	public static function send($to= "",$name= "",$subject= "",$message= ""){
		$mail = new PHPMailer();
		    //Server settings
            // Enable verbose debug output
		    $mail->isSMTP(); // Set mailer to use SMTP
			$mail->Host = 'tls://smtp.gmail.com:587';
			$mail->SMTPOptions = array(
			   'ssl' => array(
				 'verify_peer' => false,
				 'verify_peer_name' => false,
				 'allow_self_signed' => true
				)
			);
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = "";                 // SMTP username
		    $mail->Password = "";                           // SMTP password
		    $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 587 ;                      
			$mail->setFrom('office@webdirect.ro', 'Debugger Mail');
		    //Recipients
		    $mail->addAddress($to, $name);
		   		    //Content
		    #$mail->isHTML(true);                                  // Set email format to HTML
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
		    if($mail->send()){
		    	echo 'Message has been sent';
		    }else{
		    	echo 'NU*'.$mail->ErrorInfo;
		    }
	}
}

