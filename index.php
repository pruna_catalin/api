<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/25/2019
 * Time: 8:38 PM
 */
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header("Access-Control-Allow-Origin: *");
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('BASEPATH', dirname(__FILE__));
include "vendor/autoload.php";
include "Loader.php";
Loader::register();